
const express = require('express');
const app = express();
const swaggerjsdocs = require('swagger-jsdoc');
const swaggerui = require('swagger-ui-express');
const pg = require('./OperacoesBanco');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer({ storage: multer.memoryStorage() });

var port = process.env.PORT || 3000;
let options = {
  swaggerDefinition: {
    info: {
      description: 'Api rest back-end grupo delta',
      title: 'Swagger',
      version: '1.0.0',
      servers: ["http://localhost:" + port]
    },

  },
  apis: ['Servidor.js'] 
};

var swaggerdocs = swaggerjsdocs(options);
app.use('/api-docs', swaggerui.serve, swaggerui.setup(swaggerdocs));
app.use(bodyParser.urlencoded({ extended: true }));

/** 
 * @swagger
 * /usuarios: 
 *   get:
 *    description: retorna todos os usuarios no banco
 *    produces:
 *      - application/json
 *    responses:
 *     200:
 *       description: sucesso 
 */
app.get('/usuarios', async function (req, res) {
  var usuarios = await pg.BuscarUsuarios();
  res.json(usuarios);
})

/** 
* @swagger
* /user: 
*   put:
*    description: insere usuario
*    consumes:
*      - multipart/form-data
*    parameters:
*      - in: formData
*        name: foto
*        type: file
*        required: true
*        description: foto do usuario formato valido JPEG.
*      - in: formData
*        name: nome
*        type: string
*        required: true
*        description: nome do usuario.
*      - in: formData
*        name: endereco
*        type: string
*        required: true
*        description: Endereco do usuario.
*    responses:
*     201:
*       description: sucesso 
*/
var camposFormData = [{
  name: 'foto', maxCount: 1
}, {
  name: 'nome', maxCount: 1
}, {
  name: 'endereco', maxCount: 1
}];


app.put('/user', upload.fields(camposFormData), async function (req, res) {
  
  var nomeUsuario = req.body.nome;
  var endereco = req.body.endereco;
 
  
  var foto = Object.keys(req.files).length > 0 && req.files["foto"].length ==1?req.files["foto"][0].buffer:undefined;
  
  var resultadoOperacao = await pg.SalvarUsuario(nomeUsuario, endereco, foto);
  if (resultadoOperacao === true)
    res.sendStatus(201);
  else
    res.sendStatus(500);
})

/** 
* @swagger
* /user/{id}: 
*   delete:
*    parameters:
*       - in: path
*         name: id
*         required: true
*         description: id do usuario que sera deletado
*         schema:
*           type: integer
*           format: int64
*    description: deleta usuario
*    responses:
*     200:
*       description: sucesso 
*/
app.delete('/user/:id', async function (req, res) {
  var idUsuario = req.params["id"];
  var resultadoOperacao = await pg.DeletaUsuarioPorId(idUsuario);
  if (resultadoOperacao === true)
    res.sendStatus(200);
  else
    res.sendStatus(500);
})


/** 
* @swagger
* /user/{id}: 
*   post:
*    description: atualiza usuario
*    consumes:
*      - multipart/form-data
*    parameters:
*      - in: path
*        name: id
*        required: true
*        description: id do usuario que sera atualizado
*        schema:
*          type: integer
*          format: int64
*      - in: formData
*        name: foto
*        type: file
*        required: false
*        description: foto do usuario formato valido JPEG.
*      - in: formData
*        name: nome
*        type: string
*        required: false
*        description: nome do usuario.
*      - in: formData
*        name: endereco
*        type: string
*        required: false
*        description: Endereco do usuario.    
*    responses:
*     200:
*       description: sucesso 
*/
app.post('/user/:id', upload.fields(camposFormData), async function (req, res) {
  var idUsuario = req.params.id;
  
  var nomeUsuario = req.body.nome;
  var endereco = req.body.endereco;
  
  var foto = Object.keys(req.files).length > 0 && req.files["foto"].length ==1?req.files["foto"][0].buffer:undefined;
  var usuarioAtualizado = await pg.AtualizarUsuario(idUsuario, nomeUsuario, endereco, foto);
  
  if (usuarioAtualizado === true)
    res.sendStatus(200);
  else
    res.sendStatus(500);
})
app.listen(port);