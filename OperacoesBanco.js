const { Client } = require('pg');

process.env.NODE_TLS_REJECT_UNAUTHORIZED='0';
var clientPg 

var ObterClientPg = () => {
    clientPg = new Client({ connectionString: process.env.DATABASE_URL || 'postgres://ekbzbdsfedgbuw:3e85c8f8f80c243adcb64bee25750f2ce5e6b039bc774b7c89043f973a2409cc@ec2-3-233-236-188.compute-1.amazonaws.com:5432/d4hftuibsi441p', ssl: true });
}

var BuscarUsuario = async (id)=>{

    ObterClientPg();
    await clientPg.connect();
    const query = {
        name: 'buscar-usuario',
        text: 'select id,nome,endereco,translate(encode(imagem, \'base64\'), E\'\\n\', \'\') as imagem from usuario where id = $1',
        values: [id],
    }
    var resposta = await clientPg.query(query);
    
    return resposta.rows[0];
}

var BuscarUsuarios = async () => {

    ObterClientPg();
    await clientPg.connect();
    const query = {
        name: 'buscar-usuarios',
        text: 'select id,nome,endereco,translate(encode(imagem, \'base64\'), E\'\\n\', \'\') as imagem from usuario',
        values: [],
    }
    var resposta = await clientPg
        .query(query);

    clientPg.end();
    return resposta.rows;
}

var DeletaUsuarioPorId = async (id) => {

    ObterClientPg();
    await clientPg.connect();
    const query = {
        name: 'deleta-usuario',
        text: 'delete from usuario where id = $1',
        values: [id],
    }
    var linhasAfetadas =  await clientPg
    .query(query);

    clientPg.end();

    return linhasAfetadas.rowCount ==1;

}

var SalvarUsuario = async (nome, endereco, imagem)=>{
    ObterClientPg();
    await clientPg.connect();
    const query = {
        text: 'INSERT INTO usuario(nome, endereco, imagem) values ($1,$2,$3);',
        values: [nome, endereco, imagem],
    }
    var linhasAfetadas =  await clientPg
        .query(query);

    clientPg.end()

    return linhasAfetadas.rowCount ==1;
}

var AtualizarUsuario = async (id,nome, endereco, imagem)=>{

    if(nome === undefined && endereco === undefined && imagem === undefined ) return;

    var propriedades = [
        {propname:'nome',propvalue:nome}
        ,{propname:'endereco',propvalue:endereco},
        {propname:'imagem',propvalue:imagem}].filter(filtroPropriedadesNulas);


    ObterClientPg();
    await clientPg.connect();
    const query = {
        text: 'update  usuario set '+ GeradorSqlOpcional(propriedades)+' where id = $1;',
        values: [id].concat(propriedades.map(x=>x.propvalue)),
    }
    var linhasAfetadas = await clientPg
        .query(query);


    clientPg.end()

    return linhasAfetadas.rowCount ==1
}

var filtroPropriedadesNulas = (prop)=>prop.propvalue !== undefined;

var GeradorSqlOpcional = (propriedades)=>{
    var propFormatada = []
    for(let i=0;i<propriedades.length;i++){
        let propriedade = propriedades[i].propname + ' = $'+(i+2)
        propFormatada.push(propriedade)
    }
    return propFormatada.join(', ');
}

module.exports = {
    BuscarUsuario,
    BuscarUsuarios,
    SalvarUsuario,
    DeletaUsuarioPorId,
    AtualizarUsuario
}